;;; guix-channel
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tojoqk packages emacs)
  #:use-module (guix packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gtk)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define-public emacs-lucid
  (package
    (inherit emacs)
    (name "emacs-lucid")
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags)
        `(cons* "--with-x-toolkit=lucid" ,flags))))
    (inputs (append `(("libxaw" ,libxaw))
                    (alist-delete "gtk+" (package-inputs emacs))))))

(define-public emacs-next-cairo
  (package
    (inherit emacs-next)
    (name "emacs-next-cairo")
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-next)
       ((#:configure-flags flags)
        `(cons* "--with-cairo" ,flags))))
    (inputs (append `(("cairo" ,cairo))
                    (package-inputs emacs-next)))))
