;;; guix-channel
;;; Copyright © 2020, 2021 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tojoqk packages guile-xyz)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo))

(define-public guile-toot
  (package
    (name "guile-toot")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://gitlab.com/tojoqk/toot/uploads/"
                    "d94fb16d38619b15a4dac331b47ebda2"
                    "/toot-" version ".tar.gz"))
              (sha256
               (base32
                "19rmhxw1964zv59v9l6xqfz17zyvg6i7xzcrvm87qdavqs8rxccr"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile-3.0)
       ("guile-lib" ,guile-lib)
       ("gulie-json" ,guile-json-4)
       ("guile-picture-language" ,guile-picture-language)))
    (synopsis "Mastodon Client")
    (description "Toot is Mastodon Client.")
    (home-page "https://gitlab.com/tojoqk/toot")
    (license license:gpl3+)))

(define-public guile-vikalpa
  (package
    (name "guile-vikalpa")
    (version "0.1.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://gitlab.com/tojoqk/vikalpa/uploads/"
                    "a7b319b045ea5893e449cf5ad1bd084f"
                    "/vikalpa-" version ".tar.gz"))
              (sha256
               (base32
                "15g2pjgqwnym3lqrw14djfxz3852hrdwvywgc898qdlfrb5m5ac4"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ("texinfo" ,texinfo)))
    (inputs
     `(("guile" ,guile-3.0)))
    (synopsis "Proof assistant for GNU Guile")
    (description "Vikalpa is a proof assistant inspired by J-Bob.")
    (home-page "https://gitlab.com/tojoqk/vikalpa")
    (license license:gpl3+)))
