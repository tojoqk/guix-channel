;;; guix-channel
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tojoqk packages emacs-xyz)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-japanese-holidays
  (let ((commit "45e70a6eaf4a555fadc58ab731d522a037a81997")
        (revision "0"))
    (package
      (name "emacs-japanese-holidays")
      (version (git-version "1.190317" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/emacs-jp/japanese-holidays.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "041rww8ngvjmgkiviqwq6wci8wgh4bs0wjjc8v8lqpwqhbzf65jy"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/emacs-jp/japanese-holidays.git")
      (synopsis "Calendar functions for the Japanese calendar")
      (description
       "This utility defines Japanese holiday for calendar function.
This also enables to display weekends or any weekday with preferred face.")
      (license license:gpl2+))))

(define-public emacs-twittering-mode-git
  (let ((commit "114891e8fdb4f06b1326a6cf795e49c205cf9e29")
        (revision "0"))
    (package
      (inherit emacs-twittering-mode)
      (name "emacs-twittering-mode-git")
      (version (git-version "20181121" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/hayamiz/twittering-mode.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1w1p5pg3ambixhc5l7490wf5qasw3xv9qg6f0xhfsnqk44fp70ia")))))))
